package id.co.asyst.amala.sync.utils;

import org.apache.camel.Exchange;

import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {
    public void generateDateTimeNow(Exchange exchange) {
        Date date = new Date();
        SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        Date dateMin1Days = calendar.getTime();
        String srtDate = sm.format(date);
        exchange.setProperty("dateTimeNow",srtDate);
        String srtDateFormat = smf.format(date);
        exchange.setProperty("dateNow",srtDateFormat);
        String strDateMin1Days = smf.format(dateMin1Days);
        exchange.setProperty("dateMin1Days", strDateMin1Days);
    }

    public void generateMonitoringId(Exchange exchange){
        String monitoringid = GenerateId("", null,10);
        exchange.setProperty("monitoringId", monitoringid);
    }

    private static String GenerateId(String prefix, Date date, int numdigit) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
        Random rand = new Random();
        String dateFormat = "";
        if (date != null) {
            dateFormat = sdf.format(date);
        }

        String[] arr = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        String randomkey = "";

        for(int i = 0; i < numdigit; ++i) {
            int random = rand.nextInt(36);
            randomkey = randomkey + arr[random];
        }

        return prefix + dateFormat + randomkey;
    }
}