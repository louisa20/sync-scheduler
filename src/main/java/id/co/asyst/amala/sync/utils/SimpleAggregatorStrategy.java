package id.co.asyst.amala.sync.utils;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

/**
 * @author Louisa Gabriella
 * @version $Revision$, Apr 09, 2021
 * @since 1.2
 */

public class SimpleAggregatorStrategy implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        return newExchange;
    }
}